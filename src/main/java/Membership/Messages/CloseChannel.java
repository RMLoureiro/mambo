package Membership.Messages;

import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class CloseChannel extends Message {

    int SIGNATURE;
    InetSocketAddress addr;

    public CloseChannel(InetSocketAddress addr){
        this.addr = addr;
        SIGNATURE = 13;
    }
    @Override
    public ByteBuf encode() {
        return null;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
    }

    @Override
    public int getSIGNATURE() {
        return SIGNATURE;
    }

    public InetSocketAddress getAddress(){return addr;}
}
