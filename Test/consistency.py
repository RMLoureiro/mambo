import numpy as np
import networkx as nx
import math
import csv
from os import listdir
from os.path import isfile, join

def concistency(size, nodes):
    actives = [''] * nodes
    mypath = '../out/'
    graph = nx.DiGraph()
 
    logs = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    #get and trim data
    for i in range(0, nodes):
        print(i)
        file  = open('../out/' + logs[i], "r")
        data = list(csv.reader(file, delimiter = ','))
        actives[i] = data[-1]

        logs[i] = logs[i].replace('.csv', '')
        
        for j in range(0, size): 
            node = actives[i][j]
            node = node.replace('/', '').replace(' ','')
            if not node == str(-1):
                graph.add_edge(logs[i], node)
   
        file.close()

    actives = np.array(actives)

    nedges = len(graph.edges)
    edges = graph.edges
    edges = list(edges)
    
    for i in range(0, nedges):
        other = edges[i]
        if not( graph.has_edge(other[1], other[0])):
            print('inconsistent ' + str(other[1]) + ' ' + str(other[0]))
            
    print(nx.average_clustering(graph, graph.nodes, 1))
    print(nx.average_shortest_path_length(graph))

    
    
            
            
size = 5
nodes = 31

actives = concistency(size, nodes)