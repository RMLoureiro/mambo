package Membership.Messages;

import Membership.Hyparview.HPVNode;
import io.netty.buffer.ByteBuf;

import java.net.UnknownHostException;

public class AddToActive extends Message {

    int SIGNATURE;
    HPVNode node;
    Message msg;

    public AddToActive(HPVNode node, Message msg){
        this.node = node;
        this.msg = msg;
        SIGNATURE = 14;
    }
    @Override
    public ByteBuf encode() {
        return null;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
    }

    @Override
    public int getSIGNATURE() {
        return SIGNATURE;
    }

    public HPVNode GetNode(){return node;}

    public Message GetMessage(){ return msg;}
}
