package Membership.Hyparview.Handlers;

import Membership.Hyparview.Hyparview;
import Membership.Messages.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

import java.net.UnknownHostException;

public class HPVHandler extends ChannelInboundHandlerAdapter{

    Hyparview hypar;

    public HPVHandler(Hyparview hypar){
        this.hypar = hypar;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws UnknownHostException {
        hypar.ReceiveMessage(ctx.channel(), (Message) msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, java.lang.Throwable cause){
        cause.printStackTrace();
        hypar.ReceiveMessage(ctx.channel(), new DisconnectIdle());
    }

    public void userEventTriggered(ChannelHandlerContext ctx, Object evt){
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if(e.state() == IdleState.WRITER_IDLE){
                hypar.ReceiveMessage(ctx.channel(), new Ping());
            }else if(e.state() == IdleState.READER_IDLE){
                hypar.ReceiveMessage(ctx.channel(), new DisconnectIdle());
            }
        }
    }
}
