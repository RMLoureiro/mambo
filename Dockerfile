FROM openjdk:13-oracle
RUN mkdir /usr/src/mambo
COPY target/MAMBO-1.0-jar-with-dependencies.jar /usr/src/mambo
WORKDIR /usr/src/mambo