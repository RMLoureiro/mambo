package Membership.Messages;

import Membership.Hyparview.HPVNode;
import io.netty.buffer.ByteBuf;

import java.net.UnknownHostException;

public class SendNeighbourRequest extends Message {

    int SIGNATURE;
    HPVNode node;

    public SendNeighbourRequest(HPVNode node){
        this.node = node;
        SIGNATURE = 15;
    }
    @Override
    public ByteBuf encode() {
        return null;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
    }

    @Override
    public int getSIGNATURE() {
        return SIGNATURE;
    }

    public HPVNode getNode(){return node;}
}
