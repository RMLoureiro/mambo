package Membership.Hyparview;

import io.netty.channel.Channel;

import java.net.InetSocketAddress;

public class HPVNode{

    InetSocketAddress addr;
    Channel ch;
    boolean temp;


    public HPVNode(InetSocketAddress addr, Channel ch){
        this.addr = addr;
        this.ch = ch;
        this.temp = false;
    }

    public HPVNode(InetSocketAddress addr, Channel ch, boolean isTemp){
        this.addr = addr;
        this.ch = ch;
        this.temp = isTemp;
    }

    public void Accepted(){temp = false;}

    public boolean IsTemp(){ return temp;}

    public Channel GetChannel(){return ch;}

    public InetSocketAddress GetAddress(){return addr;}
}
