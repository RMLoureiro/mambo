package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Iterator;

public class ShuffleReply extends Message {

    int SIGNATURE;
    int nonce;
    HashSet<InetSocketAddress> k;
    public ShuffleReply(HashSet<InetSocketAddress> k, int nonce){
        SIGNATURE = 4;
        this.nonce = nonce;
        this.k = new HashSet<>(k);
    }

    public ShuffleReply(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 4;
        decode(msg);
    }
    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(nonce);

        tmp.writeInt(k.size());

        Iterator<InetSocketAddress> it = k.iterator();
        InetSocketAddress n;

        while(it.hasNext()){
            n = it.next();
            tmp.writeInt(n.getPort());
            tmp.writeInt(n.getAddress().getAddress().length);
            tmp.writeBytes(n.getAddress().getAddress());
        }

        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }

        nonce = msg.readInt();

        k = new HashSet<>();

        for(int i = msg.readInt(); i > 0; i--){
            int port = msg.readInt();
            int length = msg.readInt();
            byte[] ip = new byte[length];
            msg.readBytes(ip);
            InetAddress addr = InetAddress.getByAddress(ip);
            k.add(new InetSocketAddress(addr, port));
        }
    }

    public HashSet<InetSocketAddress> getK() {
        return k;
    }

    public int getNonce(){ return nonce;}

    public int getSIGNATURE(){return SIGNATURE;}
}
