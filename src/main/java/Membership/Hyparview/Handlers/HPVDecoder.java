
package Membership.Hyparview.Handlers;

import Membership.Messages.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.DecoderException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HPVDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> out) throws Exception {
        if (byteBuf.readableBytes() < Integer.BYTES) return;
        int size = byteBuf.getInt(byteBuf.readerIndex());
        if (size <= (byteBuf.readableBytes() - Integer.BYTES)) {
            ByteBuf msg = byteBuf.readSlice(size + Integer.BYTES);
            msg.readInt();
            int signature = msg.readInt();
            Message message;
            switch (signature) {
                case 1:
                    message = new FowardJoin(msg);
                    out.add(message);
                    break;
                case 2:
                    message = new Disconnect(msg);
                    out.add(message);
                    break;
                case 3:
                    message = new Shuffle(msg);
                    out.add(message);
                    break;
                case 4:
                    message = new ShuffleReply(msg);
                    out.add(message);
                    break;
                case 5:
                    message = new NeighbourRequest(msg);
                    out.add(message);
                    break;
                case 6:
                    message = new NeighbourAccept(msg);
                    out.add(message);
                    break;
                case 7:
                    message = new Join(msg);
                    out.add(message);
                    break;
                case 8:
                    message = new DisconnectIdle(msg);
                    out.add(message);
                    break;
                case 9:
                    message = new NeighbourReject(msg);
                    out.add(message);
                    break;
                case 10:
                    message = new JoinReply(msg);
                    out.add(message);
                    break;
                case 17:
                    message = new ReceivedMessage(msg);
                    out.add(message);
                    break;
                case 18:
                    new Ping(msg);
                    break;
            }

        }
    }
}
