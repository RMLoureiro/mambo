package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class NeighbourAccept extends Message{

    int SIGNATURE;
    InetSocketAddress addr;

    public NeighbourAccept(InetSocketAddress addr){
        SIGNATURE = 6;
        this.addr = addr;
    }

    public NeighbourAccept(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 6;
        decode(msg);
    }

    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(addr.getPort());
        tmp.writeInt(addr.getAddress().getAddress().length);
        tmp.writeBytes(addr.getAddress().getAddress());

        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }
        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress temp = InetAddress.getByAddress(ip);
        this.addr = new InetSocketAddress(temp, port);
    }

    public InetSocketAddress getAddr() {
        return addr;
    }

    public int getSIGNATURE(){return SIGNATURE;}
}
