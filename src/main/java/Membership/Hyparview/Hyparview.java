package Membership.Hyparview;

import Membership.Hyparview.Handlers.HPVDecoder;
import Membership.Hyparview.Handlers.HPVEncoder;
import Membership.Hyparview.Handlers.HPVHandler;
import Membership.Messages.*;
import Membership.Node;
import MessageDissemmination.Gossip;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.net.InetSocketAddress;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class Hyparview implements Node {

    private static int LOW = 0;
    private static int HIGH = 1;

    private static int contactPort;
    private static String contactIP;

    //PARAMS
    private static int ACTIVE = 5;
    private static int PASSIVE = 20;
    private static int ARWL = 6;
    private static int PRWL = 3;
    private static int KAS = 3;
    private static int KPS = 4;
    private static int shuffleTTL = 5;

    private static int port;
    private static String ip;
    private static InetSocketAddress myAddr;
    private static Hyparview self;

    ServerBootstrap sb;
    Bootstrap b;
    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;
    EventLoopGroup workerGroup2;

    Gossip gossip;

    Set<InetSocketAddress> passiveView;
    Map<InetSocketAddress, HPVNode> activeView;

    BlockingDeque<MessageReceived> messages;

    Message lastShuffle;

    public Hyparview(String ip, int port, String contactIP, int contactPort, Gossip gossip){
        self = this;
        this.gossip = gossip;

        this.port = port;
        this.ip = ip;
        this.contactIP = contactIP;
        this.contactPort = contactPort;

        myAddr = new InetSocketAddress(ip, port);
        passiveView = new HashSet<>();
        activeView = new HashMap<>();

        messages = new LinkedBlockingDeque<>();

        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        workerGroup2 = new NioEventLoopGroup();

        sb = new ServerBootstrap().group(bossGroup,workerGroup).
                channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {

            @Override
            protected void initChannel(SocketChannel ch){
                ch.pipeline().addLast("idleStateHandler", new IdleStateHandler(20, 7, 0));
                ch.pipeline().addLast("decoder", new HPVDecoder());
                ch.pipeline().addLast("encoder", new HPVEncoder());
                ch.pipeline().addLast("HPVHandler", new HPVHandler(self));
            }
        });


        ChannelFuture f;
        try {
            f = sb.bind(port).sync();
            //System.out.println(f.channel().localAddress());
        } catch (InterruptedException e) {
            e.printStackTrace();
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            workerGroup2.shutdownGracefully();
        }



        b = new Bootstrap();
        b.group(workerGroup2);
        b.channel(NioSocketChannel.class);
        b.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch){
                ch.pipeline().addLast("idleStateHandler2", new IdleStateHandler(20, 7, 0));
                ch.pipeline().addLast("decoder2", new HPVDecoder());
                ch.pipeline().addLast("encoder2", new HPVEncoder());
                ch.pipeline().addLast("HPVHandler2", new HPVHandler(self));
            }
        });

        if(contactIP != null && contactPort != 0 && !(contactIP.equals(ip) && contactPort == port)){
            InetSocketAddress n = new InetSocketAddress(contactIP, contactPort);
            b.connect(n).addListener((ChannelFutureListener) future -> {
                ChannelFuture closeFuture = future.channel().closeFuture();
                closeFuture.addListener((ChannelFutureListener) cf -> self.Close(n));
                Join(future.channel(), n);
            });
        }


        Timer timer = new Timer();
        TimerTask task = new TimerTask(){

            @Override
            public void run(){
                ReceiveMessage(null, new ShuffleTime());
            }
        };

        printViews();

        timer.schedule(task, 1000, 1000);

        while(true){
            MessageReceived msg;
            try {
                msg = messages.take();
                Receive(msg.getCh(), msg.getMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void ReceiveMessage(Channel ch, Message msg){
        MessageReceived message = new MessageReceived(ch, msg);
        messages.addLast(message);
    }


    public void Receive(Channel ch,  Message msg){
        int signature = msg.getSIGNATURE();
        HPVNode node;
        switch(signature){
            case 1:
                FowardJoin fj = (FowardJoin) msg;
                ReceiveFowardJoin(fj.getTTL(),(InetSocketAddress) ch.remoteAddress(), fj.getNewNode());
                break;
            case 2:
                Disconnect dc = (Disconnect) msg;
                DisconnectReceived(dc.getAddr(), ch);
                break;
            case 3:
                Shuffle shuffle = (Shuffle) msg;
                ShuffleReq(shuffle.getK(), shuffle.getTTL(), shuffle.getSender(), shuffle.getOrigin(), shuffle.getNonce());
                break;
            case 4:
                ShuffleReply shufflerRep = (ShuffleReply) msg;
                ShuffleReply(ch, shufflerRep.getK(), shufflerRep.getNonce());
                break;
            case 5:
                NeighbourRequest neighbourRequest = (NeighbourRequest) msg;
                NeighbourRequestReceived(ch, neighbourRequest.getPriority(), neighbourRequest.getAddr());
                break;
            case 6:
                NeighbourAccept neighbourAccept = (NeighbourAccept) msg;
                NeighbourAccept(ch, neighbourAccept.getAddr());
                break;
            case 7:
                Join join = (Join) msg;
                JoinReq(ch, join.getAddr());
                break;
            case 8:
                DisconnectIdle(ch);
                break;
            case 9:
                NeighbourReject neighbourReject = (NeighbourReject) msg;
                NeighbourReject(ch, neighbourReject.getAddr());
                break;
            case 10:
                JoinReply joinReply = (JoinReply) msg;
                JoinReply(joinReply.getAddr(), ch);
                break;
            case 12:
                printViews();
                if(activeView.size() > 0) {
                    Shuffle();
                }
                if(activeView.size() == 0 && passiveView.size() <= 1){
                    if(contactIP != null && contactPort != 0 && !(contactIP.equals(ip) && contactPort == port)){
                        InetSocketAddress n = new InetSocketAddress(contactIP, contactPort);
                        b.connect(n).addListener((ChannelFutureListener) future -> {
                            ChannelFuture closeFuture = future.channel().closeFuture();
                            closeFuture.addListener((ChannelFutureListener) cf -> self.Close(n));
                            Join(future.channel(), n);
                        });
                    }
                }
                if(activeView.size() < ACTIVE && passiveView.size() > 0){
                    FindNeighbours();
                }
                break;
            case 13:
                CloseChannel(((CloseChannel) msg).getAddress());
                break;
            case 14:
                AddToActive ata = (AddToActive) msg;
                node = ata.GetNode();
                if(activeView.containsKey(node.GetAddress())){
                    //System.out.println("NODE ADDED TO ACTIVE "+ node.GetAddress().getPort());
                    activeView.put(node.GetAddress(), new HPVNode(node.addr, node.ch));
                    if(ata.GetMessage() != null){
                        //System.out.println("SENT NODE MESSAGE");
                        node.GetChannel().writeAndFlush(ata.GetMessage());
                    }
                }
                break;
            case 15:
                node = ((SendNeighbourRequest)msg).getNode();
                SendNeighbourRequest(node.ch);
                break;
            case 16:
                UpperMessage upperMessage = (UpperMessage) msg;
                ReceivedMessage rec = new ReceivedMessage(upperMessage.GetCode(), upperMessage.GetMessage(), myAddr);
                ch.writeAndFlush(rec);
                break;
            case 17:
                ReceivedMessage receivedMessage = (ReceivedMessage) msg;
                gossip.Receive(receivedMessage.GetCode(), receivedMessage.GetMessage(), receivedMessage.GetAddress());
                break;
            case 18:
                ch.writeAndFlush(msg);
                break;

        }
    }


    void Shuffle() {
        Random rnd = new Random();
        int i = rnd.nextInt(activeView.size());
        InetSocketAddress n = null;
        for (int j = 0; j<activeView.size();j++){
            n = (InetSocketAddress) activeView.keySet().toArray()[i];
            i = rnd.nextInt(activeView.size());
            if(activeView.get(n) != null){
                break;
            }
        }
        if(activeView.get(n) == null){
            return;
        }
        HashSet<InetSocketAddress> k = new HashSet<>(activeView.keySet());

        k.remove(n);
        while(k.size() > KAS){
            i = rnd.nextInt(k.size());
            k.remove(k.toArray()[i]);
        }

        Set<InetSocketAddress> kp = new HashSet<>(passiveView);
        while(kp.size() > KPS){
            i = rnd.nextInt(kp.size());
            InetSocketAddress tmp = (InetSocketAddress) kp.toArray()[i];
            kp.remove(tmp);
            k.add(tmp);
        }

        Random random = new Random();
        int nonce = random.nextInt();
        Message msg = new Shuffle(shuffleTTL, k, myAddr, myAddr, nonce);
        lastShuffle = msg;
        Channel ch = activeView.get(n).GetChannel();
        ch.writeAndFlush(msg);
    }

    void ShuffleReq(HashSet<InetSocketAddress> k, int TTL, InetSocketAddress sender, InetSocketAddress origin, int nonce){
        TTL -= 1;

        if(TTL > 0) {
            HashSet<InetSocketAddress> tmp = new HashSet<>(activeView.keySet());

            tmp.remove(sender);
            tmp.remove(origin);

            if (!tmp.isEmpty()) {
                Random rnd = new Random();
                int i = rnd.nextInt(tmp.size());
                InetSocketAddress n = null;
                Message msg = new Shuffle(TTL, k, origin, myAddr, nonce);
                for (int j = 0; j < tmp.size();j++){
                    n = (InetSocketAddress) tmp.toArray()[i];
                    i = rnd.nextInt(tmp.size());
                    if(activeView.get(n)!= null){
                        break;
                    }
                }
                if(activeView.get(n) == null){
                    return;
                }
                Channel ch = activeView.get(n).ch;
                ch.writeAndFlush(msg);
            }
        }else {
            HashSet<InetSocketAddress> replyK = new HashSet<>();
            InetSocketAddress tmp;
            Iterator<InetSocketAddress> it = passiveView.iterator();

            while (it.hasNext() && replyK.size() < k.size()) {
                tmp = it.next();
                if (!replyK.contains(k) && !k.contains(tmp)) {
                    replyK.add(tmp);
                }
            }

            b.connect(origin).addListener((ChannelFutureListener) future -> future.channel().writeAndFlush(new ShuffleReply(replyK, nonce)));

            it = k.iterator();
            Iterator<InetSocketAddress> it2 = replyK.iterator();
            while (it.hasNext()) {
                tmp = it.next();
                if (!passiveView.contains(tmp) && !activeView.containsKey(tmp) && !tmp.equals(myAddr)) {
                    if (passiveView.size() >= PASSIVE) {
                        if (it2.hasNext()) {
                            passiveView.remove(it2.next());
                        } else {
                            DropRandomFromPassive();
                        }
                    }
                    passiveView.add(tmp);
                }
            }
        }

    }

    void ShuffleReply(Channel ch, HashSet<InetSocketAddress> k, int nonce){
        if(nonce == ((Shuffle) lastShuffle).getNonce()){
            InetSocketAddress tmp;
            InetSocketAddress tmp2;
            Iterator<InetSocketAddress> it = k.iterator();
            Iterator<InetSocketAddress> it2 = ((Shuffle) lastShuffle).getK().iterator();
            while(it.hasNext()){
                tmp = it.next();
                if(!passiveView.contains(tmp) && !activeView.containsKey(tmp) && !tmp.equals(myAddr)) {
                    while(passiveView.size() >= PASSIVE) {
                        if(it2.hasNext()){
                            tmp2 = it2.next();
                            passiveView.remove(tmp2);
                        }else {
                            DropRandomFromPassive();
                        }
                    }
                    passiveView.add(tmp);
                }
            }
        }else{
            InetSocketAddress tmp;
            Iterator<InetSocketAddress> it = k.iterator();
            while(it.hasNext()){
                tmp = it.next();
                if(!passiveView.contains(tmp) && !activeView.containsKey(tmp) && !tmp.equals(myAddr)) {
                    if(passiveView.size() >= PASSIVE) {
                        DropRandomFromPassive();
                    }
                    passiveView.add(tmp);
                }
            }
        }
        ch.close();

    }

    void JoinReq(Channel ch, InetSocketAddress addr){
        if(ch.isActive()) {
            while (activeView.size() >= ACTIVE) {
                DropRandomFromActive(null);
            }
            if (activeView.size() > 0) {
                SendFowardJoin(ARWL, myAddr, addr);
            }
            passiveView.remove(addr);
            activeView.put(addr, new HPVNode(addr, ch));
            ch.writeAndFlush(new JoinReply(myAddr));
            //System.out.println("JOIN " + addr.getPort());
        }
    }

    void ReceiveFowardJoin(int TTL, InetSocketAddress sender, InetSocketAddress newNode){
        if(((activeView.size() <= 1) || (TTL == 0)) && !myAddr.equals(newNode) && !activeView.containsKey(newNode)) {
            while (activeView.size() >= ACTIVE) {
                DropRandomFromActive(null);
            }
            //System.out.println("ACCEPTED FOWARD JOIN" + newNode.getPort());
            passiveView.remove(newNode);
            activeView.put(newNode, null);
            b.connect(newNode).addListener((ChannelFutureListener) future -> {
                ChannelFuture closeFuture = future.channel().closeFuture();
                closeFuture.addListener((ChannelFutureListener) cf -> self.Close(newNode));
                messages.addFirst(new MessageReceived(future.channel(), new AddToActive(new HPVNode(newNode, future.channel()), new JoinReply(myAddr))));
            });
        } else{
            if (TTL == PRWL){
                if(!myAddr.equals(newNode) && !activeView.containsKey(newNode) && !passiveView.contains(newNode)){
                    while(passiveView.size() >= PASSIVE) {
                        DropRandomFromPassive();
                    }
                    passiveView.add(newNode);
                }
            }
        }

        if(TTL > 0) {
            SendFowardJoin(TTL-1, sender, newNode);
        }
    }


    void SendFowardJoin(int TTL, InetSocketAddress sender, InetSocketAddress newNode){
        Set<InetSocketAddress> temp = new HashSet<>(activeView.keySet());
        temp.remove(sender);
        temp.remove(newNode);

        while(!temp.isEmpty()){

            Random rnd = new Random();
            int i = rnd.nextInt(temp.size());
            InetSocketAddress tmp = (InetSocketAddress) temp.toArray()[i];

            if(activeView.get(tmp) == null){
                temp.remove(tmp);
            }else {
                Message msg = new FowardJoin(TTL-1, newNode);
                Channel ch = activeView.get(tmp).GetChannel();
                temp.remove(tmp);
                ch.writeAndFlush(msg);
                if(TTL < ARWL){
                    return;
                }
            }
        }
    }

    void NeighbourRequestReceived(Channel ch, int priority, InetSocketAddress addr){
        if(!myAddr.equals(addr)) {
            if(!activeView.containsKey(addr)) {
                if (priority == HIGH) {
                    while (activeView.size() >= ACTIVE) {
                        DropRandomFromActive(null);
                    }
                }
                if (activeView.size() < ACTIVE) {
                    if(ch.isActive()) {
                        passiveView.remove(addr);
                        activeView.put(addr, new HPVNode(addr, ch));
                        //System.out.println("NEIGHBOUR REQUEST RECEIVED " + addr.getPort());
                        ch.writeAndFlush(new NeighbourAccept(myAddr));
                        return;
                    }else{
                        //System.out.println("CHANNEL IS INACTIVE " + addr.getPort());
                    }
                }
            }
            NeighbourReject msg = new NeighbourReject(myAddr);
            ch.writeAndFlush(msg);
        }
    }

    void NeighbourAccept(Channel ch, InetSocketAddress addr) {
        while (activeView.size() >= ACTIVE) {
            DropRandomFromActive(addr);
        }
        HPVNode node= new HPVNode(addr,ch);
        passiveView.remove(node.GetAddress());
        activeView.put(node.GetAddress(), node);
        //System.out.println("NEIGHBOUR ACCEPTED " + addr.getPort());
        gossip.NeighbourUp(addr);
    }

    void NeighbourReject(Channel ch, InetSocketAddress addr) {
        ch.close();
        passiveView.add(addr);
        FindNeighbour();
    }

    void Join(Channel ch, InetSocketAddress n){
        ch.writeAndFlush(new Join(myAddr));
        passiveView.add(n);
    }

    void JoinReply(InetSocketAddress n, Channel ch){
        while(activeView.size() >= ACTIVE){
            DropRandomFromActive(null);
        }
        //System.out.println("JOIN REPLY" + n.getPort());
        passiveView.remove(n);
        activeView.put(n, new HPVNode(n, ch));
    }

    void DisconnectIdle(Channel ch){
        Iterator<InetSocketAddress> it = new HashSet<>(activeView.keySet()).iterator();
        InetSocketAddress tmp;
        while(it.hasNext()){
            tmp = it.next();
            HPVNode node =  activeView.get(tmp);
            if(node.GetChannel().equals(ch)){
                //System.out.println("DISCONNECT IDLE" + tmp.getPort());
                activeView.remove(tmp);
                gossip.NeighbourDown(tmp);
                Message msg = new Disconnect(myAddr);
                ch.writeAndFlush(msg);
                passiveView.add(tmp);
                ch.close();
            }
        }
        FindNeighbour();
    }

    void DropRandomFromActive(InetSocketAddress addr){
        Random rnd = new Random();
        HashSet<InetSocketAddress> tmp = new HashSet<>(activeView.keySet());
        if( addr != null){
            tmp.remove(addr);
        }
        InetSocketAddress del = null;
        while(tmp.size() > 0) {
            int i = rnd.nextInt(tmp.size());
            del = (InetSocketAddress) tmp.toArray()[i];
            if(activeView.get(del) != null){
                break;
            }else{
                tmp.remove(del);
            }
        }

        if(del == null) {
            return;
        }

        Disconnect(del);
    }

    void DropRandomFromPassive(){
        Random rnd = new Random();
        int i = rnd.nextInt(passiveView.size());
        InetSocketAddress del = (InetSocketAddress) passiveView.toArray()[i];
        passiveView.remove(del);
    }

    public void Disconnect(InetSocketAddress addr){
        Message msg = new Disconnect(myAddr);
        HPVNode node = activeView.remove(addr);
        passiveView.add(addr);
        gossip.NeighbourDown(addr);

        if(node != null) {
            //System.out.println("DISCONNECT " + addr.getPort());
            Channel ch = node.GetChannel();
            ch.writeAndFlush(msg);
        }else{
            //System.out.println("DISCONNECT AND CREATE LINK " + addr.getPort());
            b.connect(addr).addListener((ChannelFutureListener) future -> {
                future.channel().writeAndFlush(msg);
            });
        }

    }

    void DisconnectReceived(InetSocketAddress addr, Channel ch){
        //System.out.println("DISCONNECT RECEIVED " + addr.getPort());
        gossip.NeighbourDown(addr);
        activeView.remove(addr);
        while(passiveView.size() >= PASSIVE)
            DropRandomFromPassive();

        passiveView.add(addr);
        ch.close();
    }

    void FindNeighbours(){
        HashSet<InetSocketAddress> tmp = new HashSet<>(passiveView);
        int j = activeView.size();
        while((j < ACTIVE) && !passiveView.isEmpty() && !tmp.isEmpty()){
            Random rnd = new Random();
            int i = rnd.nextInt(tmp.size());
            InetSocketAddress addr = (InetSocketAddress) tmp.toArray()[i];
            tmp.remove(addr);
            passiveView.remove(addr);
            b.connect(addr).addListener((ChannelFutureListener) future ->{
                ChannelFuture closeFuture = future.channel().closeFuture();
                closeFuture.addListener((ChannelFutureListener) cf -> self.Close(addr));
                messages.addFirst(new MessageReceived(future.channel(), new SendNeighbourRequest(new HPVNode(addr, future.channel(), true))));
            });
            j++;
        }
    }

    void FindNeighbour(){
        if(!passiveView.isEmpty()){
            Random rnd = new Random();
            int i = rnd.nextInt(passiveView.size());
            InetSocketAddress addr = (InetSocketAddress) passiveView.toArray()[i];
            passiveView.remove(addr);
            b.connect(addr).addListener((ChannelFutureListener) future ->{
                ChannelFuture closeFuture = future.channel().closeFuture();
                closeFuture.addListener((ChannelFutureListener) cf -> self.Close(addr));
                messages.addFirst(new MessageReceived(future.channel(), new SendNeighbourRequest(new HPVNode(addr, future.channel(), true))));
            });

        }
    }

    void SendNeighbourRequest(Channel ch){
        int priority = LOW;
        if(activeView.size() <= 1){
            priority = HIGH;
        }
        ch.writeAndFlush(new NeighbourRequest(priority, myAddr));
    }

    void Close(InetSocketAddress addr){
        ReceiveMessage(null, new CloseChannel(addr));
    }

    void CloseChannel(InetSocketAddress addr){
        if(activeView.containsKey(addr)){
            //System.out.println("CHANNEL CLOSED " + addr.getPort());
            HPVNode node = activeView.remove(addr);
            b.connect(addr).addListener((ChannelFutureListener) future -> future.channel().writeAndFlush(new Disconnect(myAddr)));
            if(node != null) {
                passiveView.add(addr);
                gossip.NeighbourDown(addr);
            }
        }
    }

    public  Iterator<InetSocketAddress> getActive(){
        return activeView.keySet().iterator();
    }

    public Iterator<InetSocketAddress> getPassive(){
        return passiveView.iterator();
    }


    void printViews(){
        Iterator<InetSocketAddress> active = getActive();
        InetSocketAddress next;

        System.out.print(new Timestamp(System.currentTimeMillis()) +", ");
        for(int i = 0; i<ACTIVE;i++){
            if (active.hasNext()) {
                next = active.next();
                System.out.print(next.getAddress().toString() + "-");
                System.out.print(next.getPort());
            }
            else{
                System.out.print("-1");
            }
            if(i<ACTIVE - 1){
                System.out.print(", ");
            }
        }

        System.out.println();

/**     Iterator<InetSocketAddress> passive = getPassive();
 for(int i = 0; i<PASSIVE;i++){
 if (passive.hasNext()) {
 next = passive.next();
 System.out.print(next.getAddress().toString() + ":");
 System.out.print(next.getPort());
 }
 else{
 System.out.print("-1");
 }
 if(i<PASSIVE - 1){
 System.out.print(", ");
 }
 }

 System.out.println();**/
    }

    @Override
    public void sendMessage(InetSocketAddress addr, long code , ByteBuf buf) {
        ReceiveMessage(activeView.get(addr).GetChannel(), new UpperMessage(code, buf, myAddr));
    }
}

