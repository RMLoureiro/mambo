package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Iterator;

public class Shuffle extends Message{

    int SIGNATURE;
    int TTL;
    int nonce;
    HashSet<InetSocketAddress> k;
    InetSocketAddress origin, sender;

    public Shuffle(int TTL, HashSet<InetSocketAddress> k, InetSocketAddress origin, InetSocketAddress sender, int nonce){
        SIGNATURE = 3;
        this.TTL = TTL;
        this.k = new HashSet<>(k);
        this.sender = sender;
        this.origin = origin;
        this.nonce = nonce;
    }

    public Shuffle(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 3;
        decode(msg);
    }
    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(TTL);

        tmp.writeInt(nonce);

        tmp.writeInt(origin.getPort());
        tmp.writeInt(origin.getAddress().getAddress().length);
        tmp.writeBytes(origin.getAddress().getAddress());

        tmp.writeInt(sender.getPort());
        tmp.writeInt(sender.getAddress().getAddress().length);
        tmp.writeBytes(sender.getAddress().getAddress());


        tmp.writeInt(k.size());

        Iterator<InetSocketAddress> it = k.iterator();
        InetSocketAddress n;
        while(it.hasNext()){
            n = it.next();
            tmp.writeInt(n.getPort());
            tmp.writeInt(n.getAddress().getAddress().length);
            tmp.writeBytes(n.getAddress().getAddress());
        }

        int size = tmp.readableBytes();
        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }

        k = new HashSet<>();

        TTL = msg.readInt();

        nonce = msg.readInt();

        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress addr = InetAddress.getByAddress(ip);
        origin = new InetSocketAddress(addr, port);

        port = msg.readInt();
        length = msg.readInt();
        ip = new byte[length];
        msg.readBytes(ip);
        addr = InetAddress.getByAddress(ip);
        sender = new InetSocketAddress(addr, port);


        for(int i = msg.readInt(); i > 0 ;i--){
            port = msg.readInt();
            length = msg.readInt();
            ip = new byte[length];
            msg.readBytes(ip);
            addr = InetAddress.getByAddress(ip);
            k.add(new InetSocketAddress(addr, port));
        }
    }

    public HashSet<InetSocketAddress> getK() {
        return k;
    }

    public int getTTL() {
        return TTL;
    }

    public int getNonce(){return nonce;}

    public InetSocketAddress getOrigin() {
        return origin;
    }

    public InetSocketAddress getSender(){
        return sender;
    }


    public int getSIGNATURE(){return SIGNATURE;}
}
