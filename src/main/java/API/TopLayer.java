package API;

import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;

public interface TopLayer {

    void NeighbourUp(InetSocketAddress addr);

    void NeighbourDown(InetSocketAddress addr);

    void Receive(ByteBuf msg, InetSocketAddress addr);

    void SendResult(long ID, int code);

    void RegisterResult(int code);
}
