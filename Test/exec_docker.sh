#!/usr/bin/env bash

OUTPUT_DIR=out
CONTACT_PORT=8000
IP=10.22.110.203
N=10
T=30
run_instance() {
  echo -ne "Starting Instance: "$1 "$2" " number: $3 \r"
  docker run -p ${1}:${1} mambo java -cp MAMBO-1.0-jar-with-dependencies.jar Main $IP $1 $IP $2 &> ../out/out${4}/${IP}-${1}.csv  &
}

rm -r ../out
mkdir ../out

for ((j=0; j<=0; j++))
  do
    echo "TEST "$j
    mkdir ../out/out${j}
    run_instance $CONTACT_PORT $CONTACT_PORT 0 $j
    sleep 2
    for ((i=1;i<=N;i++))
        do
            sleep 0.5
            node_port=$((CONTACT_PORT + i))
            run_instance  $node_port $CONTACT_PORT $i $j
        done

        echo -ne '                                                                                                                   \r'
        for ((k=0; k<=T; k++))
          do
              echo -ne $k "/" $T " \r"
              sleep 1
          done

        docker kill $(docker ps -q)
        docker rm $(docker ps -a -q)
        python3 verify.py "../out/out${j}/"

        echo "#############################################################################################################################"
  done
