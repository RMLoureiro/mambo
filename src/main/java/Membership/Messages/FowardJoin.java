package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class FowardJoin extends Message{

    int SIGNATURE;
    int TTL;
    InetSocketAddress newNode;

    public FowardJoin(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 1;
        decode(msg);
    }

    public FowardJoin(int TTL, InetSocketAddress newNode){
        SIGNATURE = 1;
        this.TTL = TTL;
        this.newNode = newNode;
    }

    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(TTL);

        tmp.writeInt(newNode.getPort());
        tmp.writeInt(newNode.getAddress().getAddress().length);
        tmp.writeBytes(newNode.getAddress().getAddress());


        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }
        TTL = msg.readInt();
        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress addr = InetAddress.getByAddress(ip);
        newNode = new InetSocketAddress(addr, port);
    }

    public InetSocketAddress getNewNode() {
        return newNode;
    }

    public int getTTL() {
        return TTL;
    }

    public int getSIGNATURE(){return SIGNATURE;}
}
