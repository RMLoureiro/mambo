package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class NeighbourRequest extends Message{

    int SIGNATURE;
    int priority;
    InetSocketAddress addr;

    public NeighbourRequest(int priority, InetSocketAddress addr){
        SIGNATURE = 5;
        this.priority = priority;
        this.addr = addr;
    }

    public NeighbourRequest(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 5;
        decode(msg);
    }

    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(priority);

        tmp.writeInt(addr.getPort());
        tmp.writeInt(addr.getAddress().getAddress().length);
        tmp.writeBytes(addr.getAddress().getAddress());

        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }
        priority = msg.readInt();

        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress temp = InetAddress.getByAddress(ip);
        addr = new InetSocketAddress(temp, port);
    }

    public int getPriority(){
        return priority;
    }

    public InetSocketAddress getAddr(){return addr;}

    public int getSIGNATURE(){return SIGNATURE;}
}
