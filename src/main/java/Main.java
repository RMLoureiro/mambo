import API.APIclass;

import java.io.IOException;
import java.security.InvalidParameterException;

public class Main {

    public static void main(String[] args) throws IOException {
        System.setProperty("java.net.preferIPv4Stack", "true");

        int port = 8080, contactPort = 0;
        String ip = null, contactIP = null;

        if(args.length == 0){
            throw new InvalidParameterException();
        }


        if (args.length > 1) {
            ip = args[0];
            port = Integer.parseInt(args[1]);
        }
        if(args.length > 2){
            contactIP = args[2];
            contactPort = Integer.parseInt(args[3]);
        }

        try{
            new APIclass(ip, port, contactIP, contactPort);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
