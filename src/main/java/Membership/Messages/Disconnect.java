package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class Disconnect extends Message {

    InetSocketAddress addr;

    int SIGNATURE;

    public Disconnect(InetSocketAddress addr){
        this.addr = addr;
        SIGNATURE = 2;
    }

    public Disconnect(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 2;
        decode(msg);
    }

    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        tmp.writeInt(addr.getPort());
        tmp.writeInt(addr.getAddress().getAddress().length);
        tmp.writeBytes(addr.getAddress().getAddress());

        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }
        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress tmp = InetAddress.getByAddress(ip);
        addr = new InetSocketAddress(tmp, port);
    }

    @Override
    public int getSIGNATURE() {
        return SIGNATURE;
    }


    public InetSocketAddress getAddr(){ return addr;}
}
