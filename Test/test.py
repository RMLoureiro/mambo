import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
import csv
import datetime

from os import listdir
from os.path import isfile, join

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

def openFiles():
    return [f for f in listdir(mypath) if isfile(join(mypath, f))]



def logsToData():
    global actives
    global logs
    global fullData
    for i in range(0, nodes):
        file = open('../out/' + logs[i], "r")
        data = list(csv.reader(file, delimiter = ','))
        actives[i] = data[-1]
        fullData[i] = data
        
        logs[i] = logs[i].replace('.csv', '')
        
        for j in range(0, size+1): 
            node = actives[i][j]
            node = node.replace('/', '').replace(' ','')
            if not node == str(-1):
                graph.add_edge(logs[i], node)
           
        file.close()
        
    actives = np.array(actives)
        

size = 5
nodes = 50
mypath = '../out/'
nedges = -1
edges = None
graph = nx.DiGraph()
actives = [''] * nodes
fullData= [''] * nodes

logs = openFiles()
logsToData()
print(fullData[0][0][0])
