package Membership;

import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;

public interface Node {
    void sendMessage(InetSocketAddress addr, long code, ByteBuf buf);
}
