package Membership.Messages;

import io.netty.channel.Channel;

public class MessageReceived {
    Message message;
    Channel ch;

    public MessageReceived(Channel ch, Message message){
        this.message = message;
        this.ch = ch;
    }

    public Channel getCh(){return ch;}

    public Message getMessage(){return message;}
}
