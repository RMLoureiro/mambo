package API;

import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;
import java.util.Set;

public interface API {

    void Send(long code, ByteBuf msg, InetSocketAddress addr);

    Set<InetSocketAddress> Register(TopLayer ref, long code);

    void NeighbourUp(InetSocketAddress addr);

    void NeighbourDown(InetSocketAddress addr);

    void Receive(long code, ByteBuf msg, InetSocketAddress addr);
}
