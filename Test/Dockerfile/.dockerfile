FROM openjdk:13-oracle
COPY target/MAMBO-1.0-jar-with-dependencies.jar usr/src/mambo
WORKDIR /usr/src/mambo
RUN java -cp target/MAMBO-1.0-jar-with-dependencies.jar Main 127.0.0.1 8000 127.0.0.1 8000 &> ../out/out0/127.0.0.1-8000.csv  &
CMD ["java", "Main"]