package MessageDissemmination;

import API.API;
import Membership.Hyparview.Hyparview;
import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;
import java.util.Set;

public class Gossip {

    Hyparview hpv;
    API api;
    Set<InetSocketAddress> view;
    public Gossip(API api, String ip, int port, String contactIP, int contactPort){
        this.api = api;
        this.hpv = new Hyparview(ip, port, contactIP, contactPort, this);
    }

    public void Send(long code, ByteBuf msg, InetSocketAddress addr) {
        hpv.sendMessage(addr, code, msg);
    }

    public void Receive(long code, ByteBuf msg, InetSocketAddress addr){
        api.Receive(code, msg, addr);
    }

    public void NeighbourUp(InetSocketAddress addr){ api.NeighbourUp(addr);}

    public void NeighbourDown(InetSocketAddress addr){
        api.NeighbourDown(addr);
    }

    public Set<InetSocketAddress> GetView(){
        return view;
    }
}
