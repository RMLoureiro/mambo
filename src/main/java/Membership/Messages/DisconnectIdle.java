package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.UnknownHostException;

public class DisconnectIdle extends Message {


    int SIGNATURE;
    public DisconnectIdle(){
        SIGNATURE = 8;
    }

    public DisconnectIdle(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 8;
        decode(msg);
    }
    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeInt(MAGIC);

        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {
        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }

    }

    public int getSIGNATURE(){return SIGNATURE;}
}
