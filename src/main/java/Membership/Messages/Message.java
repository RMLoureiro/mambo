package Membership.Messages;

import io.netty.buffer.ByteBuf;

import java.net.UnknownHostException;

public abstract class Message {
    int MAGIC = 42;

    public abstract ByteBuf encode();

    public abstract void decode(ByteBuf msg) throws UnknownHostException;

    public abstract int getSIGNATURE();

}
