package API;

import MessageDissemmination.Gossip;
import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class APIclass implements API{


    static Gossip gossip;
    Map<Long, TopLayer> topLayers;

    public APIclass(String ip, int port, String contactIP, int contactPort){
        topLayers = new HashMap<Long, TopLayer>();
        gossip = new Gossip(this, ip, port, contactIP, contactPort);
    }



    public void Send(long code, ByteBuf msg, InetSocketAddress addr) {
        gossip.Send(code, msg, addr);
    }

    @Override
    public Set<InetSocketAddress> Register(TopLayer ref, long code) {
        topLayers.put(code, ref);
        return gossip.GetView();
    }

    public void Receive(long code, ByteBuf msg, InetSocketAddress addr){
        topLayers.get(code).Receive(msg, addr);
    }

    public void NeighbourUp(InetSocketAddress addr){
        topLayers.forEach( (code, topLayer) ->
                topLayer.NeighbourUp(addr));
    }

    public void NeighbourDown(InetSocketAddress addr){
        topLayers.forEach( (code, topLayer) ->
                topLayer.NeighbourDown(addr));
    }
}

