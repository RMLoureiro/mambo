package Membership.Messages;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class UpperMessage extends Message {

    int SIGNATURE;
    ByteBuf msg;
    long code;
    InetSocketAddress sender;

    public UpperMessage(long code, ByteBuf msg, InetSocketAddress addr){
        this.code = code;
        this.msg = msg;
        this.sender = addr;
        SIGNATURE = 16;
    }

    public UpperMessage(ByteBuf msg) throws UnknownHostException {
        SIGNATURE = 16;
        decode(msg);
    }

    @Override
    public ByteBuf encode() {
        ByteBuf tmp = Unpooled.buffer();
        ByteBuf msg = Unpooled.buffer();

        tmp.writeInt(SIGNATURE);

        tmp.writeLong(code);

        tmp.writeInt(MAGIC);

        tmp.writeInt(sender.getPort());
        tmp.writeInt(sender.getAddress().getAddress().length);
        tmp.writeBytes(sender.getAddress().getAddress());

        tmp.writeBytes(this.msg);
        msg.writeInt(tmp.readableBytes());
        msg.writeBytes(tmp);
        tmp.release();
        return msg;
    }

    @Override
    public void decode(ByteBuf msg) throws UnknownHostException {

        int magic = msg.readInt();
        if(magic != MAGIC){
            System.out.println("asdasdasdasd");
        }

        this.code = msg.readInt();

        int port = msg.readInt();
        int length = msg.readInt();
        byte[] ip = new byte[length];
        msg.readBytes(ip);
        InetAddress addr = InetAddress.getByAddress(ip);
        sender = new InetSocketAddress(addr, port);

        this.msg = msg.readBytes(msg.readableBytes()).duplicate();
    }

    @Override
    public int getSIGNATURE() {
        return SIGNATURE;
    }

    public long GetCode(){return code;}

    public ByteBuf GetMessage(){ return msg;}

    public InetSocketAddress GetAddress(){return sender;}

}
